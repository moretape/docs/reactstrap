const fs = require("fs");
const path = require("path");
const pkg = require("./package.json");
const reactstrapPkg = require("./reactstrap/package.json");
const replace = require("replace-in-file");

const build = async () => {
  pkg.version = reactstrapPkg.version;
  fs.writeFileSync(
    path.join(__dirname, "./package.json"),
    JSON.stringify(pkg, null, 2)
  );

  try {
    const replaceReactstrap = await replace({
      files: "./examples/**/*.js",
      from: " from 'reactstrap';",
      to: " from '@moretape/ui-react';"
    });
    if (replaceReactstrap) {
      console.log(
        "✔︎ Replaced 'reactstrap' with '@moretape/ui-react' successfully"
      );
    }

    const replaceSrc = await replace({
      files: "./examples/**/*.js",
      from: " from '../../../src/",
      to: " from '@moretape/ui-react/dist/"
    });
    if (replaceSrc) {
      console.log(
        "✔︎ Replaced '../../../src/' with '@moretape/ui-react/dist/' successfully"
      );
    }

    const replaceExamples = await replace({
      files: "./examples/**/*.js",
      ignore: "./examples/**/*.es6.js",
      from: /[^]+/,
      to: (match, id, content, filePath) => {
        const paths = filePath.split("/");
        const name = paths[paths.length - 1].replace(".js", "");
        const footer = `export { default as RawCode } from '!raw-loader!./${name}.es6';`;

        if (!match.includes(footer)) {
          return `${match}\n\n${footer}`;
        }

        return match;
      }
    });
    if (replaceExamples) {
      console.log("✔︎ Added 'RawCode' exports successfully");
    }
  } catch (error) {
    console.error("Error occurred:", error);
  }
};

build();
